#pragma once

#include <string>

class Personne {
public:
    Personne(const std::string& nom);
    const std::string& getNom() const;

private:
    std::string nom;
};
