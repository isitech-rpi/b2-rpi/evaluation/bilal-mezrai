#include "Personne.h"

Personne::Personne(const std::string& nom) : nom(nom) {}

const std::string& Personne::getNom() const {
    return nom;
}
