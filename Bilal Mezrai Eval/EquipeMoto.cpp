#include "EquipeMoto.h"

EquipeMoto::EquipeMoto(const std::string& nomEquipe, const std::string& nomManager)
    : nom(nomEquipe), manager(new Personne(nomManager)) {

    lesPilotes[0] = nullptr;
    lesPilotes[1] = nullptr;
    lesPilotes[2] = nullptr;

}

EquipeMoto::~EquipeMoto() {
    delete manager;
}

const std::string& EquipeMoto::getNom() const {
    return nom;
}

void EquipeMoto::setNom(const std::string& nouveauNom) {
    nom = nouveauNom;
}

const Personne* EquipeMoto::getManager() const {
    return manager;
}

void EquipeMoto::AddPilote(unsigned int rang, Personne* pilote) {
    if (rang < 3) {
        lesPilotes[rang] = pilote;
    }
}

Personne** EquipeMoto::GetPilotes() {
    return lesPilotes;
}
