#pragma once

#include <string>
#include "Personne.h"

class EquipeMoto {
public:
    EquipeMoto(const std::string& nomEquipe, const std::string& nomManager);
    ~EquipeMoto();
    const std::string& getNom() const;
    void setNom(const std::string& nouveauNom);
    const Personne* getManager() const;
    void AddPilote(unsigned int rang, Personne* pilote);
    Personne** GetPilotes();

private:
    std::string nom;
    Personne* manager;
    Personne* lesPilotes[3];
};
