#include <iostream>
#include "Personne.h"
#include "EquipeMoto.h"

void AffichePersonne(const Personne& personne) {
    std::cout << "Personne(" << &personne << "): " << personne.getNom() << std::endl;
}

Personne* CreerPersonne() {
    std::string nom;
    std::cin >> nom;
    return new Personne(nom);
}

void AfficheEquipeMoto(EquipeMoto* equipe) {
    std::cout << "Equipe " << equipe->getNom() << " manage par " << equipe->getManager()->getNom() << std::endl;
    for (int i = 0; i < 3; ++i) {
        Personne* pilote = equipe->GetPilotes()[i];
        if (pilote) {
            std::cout << "-> Pilote[" << i << "] = " << pilote->getNom() << std::endl;
        }
        else {
            std::cout << "-> Pas de pilote[" << i << "]" << std::endl;
        }
    }
}

int main() {
    Personne pilote_1("Fabio");
    std::cout << &pilote_1 << " " << pilote_1.getNom() << std::endl;

    Personne* pilote_2 = CreerPersonne();

    AffichePersonne(pilote_1);
    AffichePersonne(*pilote_2);

    EquipeMoto* equipe_1 = new EquipeMoto("YMF", "Jarvis");
    std::cout << equipe_1 << " " << equipe_1->getNom() << " " << equipe_1->getManager()->getNom() << std::endl;

    equipe_1->AddPilote(0, &pilote_1);
    equipe_1->AddPilote(1, pilote_2);

    AfficheEquipeMoto(equipe_1);

    delete pilote_2;
    delete equipe_1;

}

